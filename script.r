# Load the package required to read JSON files.
library("rjson")

# Give the input file name to the function.
rawData <- read.csv(file="/home/sslpo/Downloads/my_data.csv", header=TRUE, sep=",")
userData <- read.csv(file="/home/sslpo/Downloads/UserLogs.csv", header=TRUE, sep=",")

  #y <- as.numeric(unlist(time2))
  #json_data[[i]]$UNSUCCESSLOGIN <- sum(y*c(60,1))
unsuccesslogin = c()
successlogin = c()
for (i in 1:length(rawData$UNSUCCESSLOGIN))
{
  time <- strsplit(toString(rawData$UNSUCCESSLOGIN[i]),":")
  #apply(time,1,trimws)
  y <- as.numeric(unlist(time))
  print(y)
  x <- sum(y*c(60,1))
  typeof(x)
  unsuccesslogin[i] <- x
}
for (i in 1:length(rawData$SUCCESSLOGIN))
{
  time <- strsplit(toString(rawData$SUCCESSLOGIN[i]),":")
  #apply(time,1,trimws)
  y <- as.numeric(unlist(time))
  print(y)
  x <- sum(y*c(60,1))
  typeof(x)
  successlogin[i] <- x
}
rawData$SUCCESSLOGIN <- successlogin
rawData$UNSUCCESSLOGIN <- unsuccesslogin
print(rawData)
image21Data <- rawData[rawData["SCHEME"]=="image21",]
text21Data <- rawData[rawData["SCHEME"]=="text21",]
hist(image21Data$NUMBEROFLOGINS, 
     main="Histogram for NUMBEROFLOGINS for image21", 
     xlab="Number of logins", 
     border="blue", 
     col="green",
     xlim=c(0,40),
     las=1, 
     breaks=5)
hist(text21Data$NUMBEROFLOGINS, 
     main="Histogram for NUMBEROFLOGINS for text21", 
     xlab="Number of logins", 
     border="blue", 
     col="green",
     xlim=c(0,40),
     las=1, 
     breaks=5)
## Summary Statistics
summaryStats <- data.frame(Scheme = character(), Variable= character() ,SD = integer(), Mean = integer(), Median = integer(),stringsAsFactors=FALSE)
# text 21 stats --
text21std_numberOfLogins <- sd(text21Data$NUMBEROFLOGINS) 
text21mean_numberOfLogins <- mean(text21Data$NUMBEROFLOGINS)
text21median_numberOfLogins <- median(text21Data$NUMBEROFLOGINS)
scheme <- "text21"
statistic <- "NUMBEROFLOGINS"
text21_numberOfLogins <- c(scheme,statistic,text21std_numberOfLogins,text21mean_numberOfLogins,text21median_numberOfLogins)
summaryStats[nrow(summaryStats) + 1,] =text21_numberOfLogins
#--SUCCESSLOGIN
text21std_successlogin <- sd(text21Data$SUCCESSLOGIN) 
text21mean_successlogin <- mean(text21Data$SUCCESSLOGIN)
text21median_successlogin <- median(text21Data$SUCCESSLOGIN)
scheme <- "text21"
statistic <- "SUCCESSLOGIN"
text21_successlogin <- c(scheme,statistic,text21std_successlogin,text21mean_successlogin,text21median_successlogin )
summaryStats[nrow(summaryStats) + 1,] =text21_successlogin
#--UNSUCCESSLOGIN
text21std_unsuccesslogin <- sd(text21Data$UNSUCCESSLOGIN) 
text21mean_unsuccesslogin <- mean(text21Data$UNSUCCESSLOGIN)
text21median_unsuccesslogin <- median(text21Data$UNSUCCESSLOGIN)
scheme <- "text21"
statistic <- "UNSUCCESSLOGIN"
text21_unuccesslogin <- c(scheme,statistic,text21std_unsuccesslogin,text21mean_unsuccesslogin,text21median_unsuccesslogin )
summaryStats[nrow(summaryStats) + 1,] =text21_unuccesslogin
#*******************************************************************************************
# image21 stats --
image21std_numberOfLogins <- sd(image21Data$NUMBEROFLOGINS) 
image21mean_numberOfLogins <- mean(image21Data$NUMBEROFLOGINS)
image21median_numberOfLogins <- median(image21Data$NUMBEROFLOGINS)
scheme <- "image21"
statistic <- "NUMBEROFLOGINS"
image21_numberOfLogins <- c(scheme,statistic,image21std_numberOfLogins,image21mean_numberOfLogins,image21median_numberOfLogins)
summaryStats[nrow(summaryStats) + 1,] =image21_numberOfLogins
#--SUCCESSLOGIN
image21std_successlogin <- sd(image21Data$SUCCESSLOGIN) 
image21std_successlogin <- sd(image21Data$SUCCESSLOGIN) 
image21mean_successlogin <- mean(image21Data$SUCCESSLOGIN)
image21median_successlogin <- median(image21Data$SUCCESSLOGIN)
statistic <- "SUCCESSLOGIN"
image21_successlogin <- c(scheme,statistic,image21std_successlogin,image21mean_successlogin,image21median_successlogin )
summaryStats[nrow(summaryStats) + 1,] =image21_successlogin
#--UNSUCCESSLOGIN
image21std_unsuccesslogin <- sd(image21Data$UNSUCCESSLOGIN) 
image21std_unsuccesslogin <- sd(image21Data$UNSUCCESSLOGIN) 
image21mean_unsuccesslogin <- mean(image21Data$UNSUCCESSLOGIN)
image21median_unsuccesslogin <- median(image21Data$UNSUCCESSLOGIN)
statistic <- "UNSUCCESSLOGIN"
image21_unsuccesslogin <- c(scheme,statistic,image21std_unsuccesslogin,image21mean_unsuccesslogin,image21median_unsuccesslogin )
summaryStats[nrow(summaryStats) + 1,] =image21_unsuccesslogin
# Some box plot

boxplot(image21Data$UNSUCCESSLOGIN,text21Data$UNSUCCESSLOGIN,
        main = "Mean unsuccessful login for image21 and text21",
        xlab = "Unsuccessful login time",
        at = c(1,2),
        names = c("image21", "text21"),
        las = 2,
        col = c("orange","red"),
        border = "brown",
        horizontal = TRUE,
        notch = TRUE
)
boxplot(image21Data$SUCCESSLOGIN,text21Data$SUCCESSLOGIN,
        main = "Mean successful login for image21 and text21",
        xlab = "Successful login time",
        at = c(1,2),
        names = c("image21", "text21"),
        las = 2,
        col = c("green","blue"),
        border = "brown",
        horizontal = TRUE,
        notch = TRUE
)
unsuccesslogin = c()
successlogin = c()
for (i in 1:length(userData$UNSUCCESSLOGIN))
{
  time <- strsplit(toString(userData$UNSUCCESSLOGIN[i]),":")
  #apply(time,1,trimws)
  y <- as.numeric(unlist(time))
  print(y)
  x <- sum(y*c(60,1))
  typeof(x)
  unsuccesslogin[i] <- x
}
for (i in 1:length(userData$SUCCESSLOGIN))
{
  time <- strsplit(toString(userData$SUCCESSLOGIN[i]),":")
  #apply(time,1,trimws)
  y <- as.numeric(unlist(time))
  print(y)
  x <- sum(y*c(60,1))
  typeof(x)
  successlogin[i] <- x
}
userData$SUCCESSLOGIN <- successlogin
userData$UNSUCCESSLOGIN <- unsuccesslogin
userData <- userData[userData["SCHEME"]=="pec_2019",]
## Summary Statistics
userStats <- data.frame(Scheme = character(), Metric= character() ,SD = integer(), Mean = integer(), Median = integer(),stringsAsFactors=FALSE)
# pec_2019 stats --
userDatastd_numberOfLogins <- sd(userData$NUMBEROFLOGINS) 
userDatamean_numberOfLogins <- mean(userData$NUMBEROFLOGINS)
userDatamedian_numberOfLogins <- median(userData$NUMBEROFLOGINS)
scheme <- "pec_2019"
statistic <- "NUMBEROFLOGINS"
userData_numberOfLogins <- c(scheme,statistic,userDatastd_numberOfLogins,userDatamean_numberOfLogins,userDatamedian_numberOfLogins)
userStats[nrow(userStats) + 1,] =userData_numberOfLogins
#--SUCCESSLOGIN
userDatastd_successlogin <- sd(userData$SUCCESSLOGIN) 
userDatamean_successlogin <- mean(userData$SUCCESSLOGIN)
userDatamedian_successlogin <- median(userData$SUCCESSLOGIN)
scheme <- "pec_2019"
statistic <- "SUCCESSLOGIN"
userData_successlogin <- c(scheme,statistic,userDatastd_successlogin,userDatamean_successlogin,userDatamedian_successlogin )
userStats[nrow(userStats) + 1,] =userData_successlogin
#--UNSUCCESSLOGIN
userDatastd_unsuccesslogin <- sd(userData$UNSUCCESSLOGIN) 
userDatamean_unsuccesslogin <- mean(userData$UNSUCCESSLOGIN)
userDatamedian_unsuccesslogin <- median(userData$UNSUCCESSLOGIN)
scheme <- "pec_2019"
statistic <- "UNSUCCESSLOGIN"
userData_unuccesslogin <- c(scheme,statistic,userDatastd_unsuccesslogin,userDatamean_unsuccesslogin,userDatamedian_unsuccesslogin )
userStats[nrow(userStats) + 1,] =userData_unuccesslogin

boxplot(image21Data$UNSUCCESSLOGIN,userData$UNSUCCESSLOGIN,
        main = "Mean unsuccessful login for image21 and pec_2019",
        xlab = "Unsuccessful login time",
        at = c(1,2),
        names = c("image21", "pec_2019"),
        las = 2,
        col = c("orange","red"),
        border = "brown",
        horizontal = TRUE,
        notch = TRUE
)
boxplot(image21Data$SUCCESSLOGIN,userData$SUCCESSLOGIN,
        main = "Mean successful login for image21 and pec_2019",
        xlab = "Successful login time",
        at = c(1,2),
        names = c("image21", "pec_2019"),
        las = 2,
        col = c("green","blue"),
        border = "brown",
        horizontal = TRUE,
        notch = TRUE
)
hist(userData$NUMBEROFLOGINS, 
     main="Histogram for Successful Login times for pec_2019", 
     xlab="Number of logins", 
     border="blue", 
     col="green",
     xlim=c(0,10),
     las=1, 
     breaks=5)