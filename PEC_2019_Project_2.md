# Quantitative Usability Evaluation: Image Based Passwords

## PEC2019 - Project Team

**Table 1:** PEC2019 Project team
| Name | Student Number | Task |
| ---------------- | -------------- | ------------------------------------------------- |
| Caleb Tony-Enwin | 100939996 | Text21 Analysis, Development and Documentation |
| Ebunoluwa Segun | 100985893 | Image21 Analysis, Questionnaire and Documentation |
| Poe Kgengwenyane | 100809178 | R program, Documentation |
| Yves Lebon Ganza | 101053186 | Data Processing and Documentation |

- To run the code go to this link [here](https://comp3008-eefb6.firebaseapp.com/). That will take you to a live website hosting the application.
- The source code for the project can be found [here](https://gitlab.com/dinecaleb/comp-3008-). To view the entire source code, you may have to send a request to Caleb Tony-Enwin, who can be contact through the school email at this [address](mailto:calebtonyenwin@cmail.carleton.ca)

---

## Usable Security

This document outlines the means by which we analyze and compare different password schemes and using the finding, we develop a new knowledge based password scheme which builds on some of the limitation we found during analysis. Passwords are how systems, local and hosted, authenticate users in order to verify their identity. The difficulty we face when writing client facing software is that of deployability, security and usability. In terms of deployability, our application is developed using firebase for all back-end requirements and the Angular Java for front end HTML rendering. The security and usability aspects will be the primary purpose of this document. In general, we want our system to be usable, which incorporates aspects such as learnability, ease of use, interface design and memorability.

<center><img src="https://infosanity.files.wordpress.com/2010/06/triad_secfuncease.png" width="50%" height="50%"></center>
<center>Figure 1: Info-Sec triad showing the relationship between usability(ease if yse), functionality and security</center>

Text based authentication is the most widely used schemes. Performance wise, we found that it was easier to use and allowed for faster login times. This come at some cost as stated in class, wide usage lead to the development of text-based password cracking which can successfully guess $50\%$ of all user chosen passwords and successfully brute-force passwords below a certain length regardless of whether they are user chosen or randomly generated.

Users use what is easy to use. We also want our system to be secure. Typical the two aspect are inversely proportional. For example, large strings of pseudo-random characters are more secure than user-chosen passwords, but these large character strings are hard to learn and remember and sometimes even require the user to carry with the additional [hardware](https://www.yubico.com/products/yub+ikey-hardware/) to do the remembering for them. In class we learned how images are typically easier to remember than character strings, by analyzing the data from [this](https://ifca.ai/fc12/pre-proceedings/paper_72.pdf) study, we also found that the added memorability of image based schemes comes with some usability issues, namely:

- Complex interface design: To maintain security the password space is an _8x6_ grid which made for difficult recollection of learned passwords.
- Increased task completion time: On average the, from start to finish, it took users more than twice as long to complete authentication tasks using image21 that when using the text based system, text21.
- Increased learning time

It is also interesting to observe that task completion time as measured in our studies and the aformentioned studies does have an impact on security. [Shoulder surfacing](<https://en.wikipedia.org/wiki/Shoulder_surfing_(computer_security)>) is a common and one of the most basic forms of [social engineering](https://www.symantec.com/connect/articles/social-engineering-fundamentals-part-i-hacker-tactics). Shoulder surfing is simply attempting to gain personal information of a user by physical observing them as they authenticate to a computer provided service. Longer task completion times exposes users to more of such attacks.

In order to address these issues, we proposed another image based password scheme, which is described in detail in the second section of this document.

## Text21

The text21 password scheme is system that creates an auto-generated random text password with the condition(az09-5). With this scheme, users are supposed to remember whatever password was generated for them. For each field, users are giving the option to test the passwords they have to ensure that it matches the generated password before accepting it. Users are later on asked to enter the passwords for each field and are allowed three attempts per field.

<center><img src="doc/text21_password_scheme.png" ></center>
<center>Figure 2: Enter password screen for the text21 schem</center>

### Text21 Advantages[Punctuation: ':']

1. Passwords are more secure than user typed password since they are random
2. Users can easily generate a password without having to think about it
3. Users are giving 3 attempts to try the passwords and also allowed to test passwords before accepting it
4. Passwords are only 5 characters long

1) Passwords are easy to forget since they are random text.
2) There might be a chance of the same password generated more than once
3) Users cannot use passwords that they choose or want
4) Password management will be difficult, in a situation when this scheme is used for multiple fields.

## Image21 Password Scheme

<center><img src="doc/image21_password_creation.png" ></center>
<center>Figure 3: Image21 password creation screen</center>

The Image21 password scheme involves the use of an image spread out on small tiles in 8 X 6 grid system. To create a password, a user is shown an image with randomly selected tiles (from the whole image). The user is meant to memorize the selected tiles and later when asked to enter the password, the user is meant to select the specific tiles, through recall, that make up the password earlier created.
This password scheme is a deviation from the regular text passwords; it takes advantage of the picture superiority effect that states that humans tend to remember images better than text. This new password scheme comes with its own advantages and disadvantages:

### Image21 Advantages

1. Independent of order: The tiles that make up a user’s password can be selected and reproduced in no particular order. In Fig. 1, the user can create the same password irrespective of the permutation of the chosen tiles.
2. Forming mental notes: The use of images makes it easier for a user to commit password to memory (based on the picture superiority effect). Individuals with photographic memories will find this especially useful.
3. Pattern formation: A user can form patterns or use monumental parts of an image as a mechanism for remembering password. For example, in Fig. 1, the user can easily remember that one tile for the password falls on the sign post.

### Image21 Disadvantages

1. Security: This password scheme may not be the most secure as a user might be tempted to take a picture or screenshot of the highlighted tiles when creating their password.
2. Difficulty in recalling: When entering a password, it is more difficult to recall what tiles were chosen since they are not highlighted (bordered) as seen when the password was first created as seen in Fig. 2.

## Scheme Comparison

The usability the image21 and text21 password schemes are compared using descirpive statistics in the R programming language. The script can be found [here](/script.r).
**Table 2:** Comparative statistics of the text21 and image21 password schemes. The histograms show the relative differences between the number of logins of each screen. The text21 hostogram shows a semi-normal distribution while the image21 data is left skewed.
| Text 21 | Image 21 |
| :----------------------------: | :-------------------------------: |
| ![](doc/text21.png) | ![](doc/image21.png) |
| ![](doc/boxplot_successul.png) | ![](doc/boxplot_unsuccessful.png) |

Summarized points:

- text21 successful login time is on average 2.7 times shorter than that of image21. This means that as a user interface, text21 allowed the user to authenticate with the testing system faster than the image21 scheme. Time spent on the interface can be attributed to a lack of understanding, poor design or simply a lengthier process. These sepearate factors cannot be thoroughly measured or interpreted from the study results. It is also important to note that the most ubiquitous password scheme is text based, for both assigned and user chosen passwords. This fact means that given an interface, the average user immediately knows what to do without instruction. Given the data and the scope of this project it is impossible to say whether the login time would decrease if image based password schemes had the same widespread usage as text based schemes.
- text21 has 16.61 number of logins compared to 19.4 for the image21 values. The two schemes showed similar variation in number of logins.
- image21 unsuccessful login times where on average 2.3 times greater than those of text21. This observation confirms the observations made from the successful login time comparisons made two bullet points above. This also offsets the benefit gained from allowing user to add the tiles used as a password in an independent order. The larger error margin compromises security while not neccessarily benefitial for the users experience.

Considering the values stated above the box-plots above it can be seen that the image based scheme quantitavely under performs when compared to the text based scheme. This can be attributed to the larger interface views required by image based password scheme, an issue observed when designing our password scheme.

**Table 3**: Summary statistics reflecting what can be seen on the boxplots and histograms, the data is discussed in the paragraph above.

<center><img src="doc/summary_data.png" width="50%" height="50%"></center>

## Visual Category Password Scheme

<center><img src="doc/Screenshot_2019-04-06_13-11-16.png" width="50%" height="50%"></center>
<center>Figure 4: Password Categories</center>

The figure directly above shows the distribution of themes from which the assigned passwords will be generated. The idea is to allow the user to select an area of interest which will allow them to more readily recall their password and obscure any brute force attacks on the password.

### Usability through Emotional Interaction and Password space expansion and User feedback

#### Emotional Interaction through Imagery

This password scheme is based on what the user knows. What the user knows is their preference of password category, which they set at the password generation inteface shown in the pictures below. The separation of passwords into categories serves two purposes. The primary purpose is to leverage user emotions to enhance usability. If the user is given a choice and allowed to select a category, there will exist some bias as to their category of choice. The five categories are, Cars, Fruit, Flags, Social Media and Places.

<center><img src="doc/generated_password.png" width="50%" height="50%"></center>
<center>Figure 5: Password generation screen</center>

A simple scenario would be that if a user set their password using our system, and proceeded to spend a week or more not loging into the service which used the password scheme, it would become dificult to retrieve the exact sequence of images from long term memory having not made use of those nueral networks over a significant period of time. This is immediately evident from the data above where the image21 data shows that the successful login time for that scheme was significantly longer than the text21 scheme. The image21 had outliers and upper quartile values hundreds of seconds greater than the text21 data, on average the login time for both successful and unsuccessful attempts was twice as much for the image data as that of the text21. Emotional interaction states that emotional cues, even subtle ones, can trigger faster recollection and even fascilitate learning. So given the case where the user does not remember the password they set, providing categories without a clue as to what category they had chosen can speed up learning and provide a vital seeding point from whence to begin at future login points. This would be particulalry useful to user who may have some dificulty with memory recollection.

#### Categories and Password Space

The secondary purpose of categorized passowords is expanding the password space. The previous point highlighted how we could potentially reduce login time for image based password schemes. Another limitation to address is the security aspect of the password scheme. The image21 scheme uses an _8x6_ grid to which presents users which more tiles than they can commit to short-term memory. This is necessary, given the design of the system, to keep a large password space. The effects on usability are detremental as shown by the data table above. Usability in this case is based on how easy it is to recall picture, particularly pictures associated with a favorite item or category. To maintain this usablity without defeating the purpose of passwords we need to abstract away the password space from the user and a potential attacker. To do this we separate the scheme into categories which the user can chose from and have the password generated using a pseudo random formula.

The password is generated from a collection of 10 images. Having a length of five the password space becomes:

<center><img src="doc/password_space.png" width="20%" height="20%"></center>

The system uses the inbuilt JavaScript random function. The following code snippen shows how the password is generated.

```javascript
  createPass(data) {
    this.category = this.getRandomImg(data);
    var arr: any = [];
    this.category.forEach(element => {
      var title = element.title;
      arr.push({
        name: title,
        letter: this.randomLetter(title)
      });
    });
    var password = (
      arr[0].letter +
      arr[1].letter +
      arr[2].letter +
      arr[3].letter
    )
      .toString()
      .toLowerCase();
    arr.push({ password: password });
    this.password = password;
    catService.info(() => `User CREATING PASSWORD`);
  }
```

In the above code snippet calls the `getRandomImg()` function which generates random images that are further used as seed for the randomly generated recovery password.

```javascript
  getRandomImg(arr) {
    var result = [];
    for (var i = 0; i < 4; i++) {
      var index = Math.floor(Math.random() * arr.length);
      result.push(arr[index]);
      arr.splice(index, 1);
    }
    return result;
  }
```

#### User Interactive Feedback

The image based system used by image21 password scheme recorded long failed login times, which we can interpret simply as the users spent a large amount of time not knowing what was going on. Two of our group members conducted tests on image21 but using the testing framework. It then became evident that a possible reason for this is that the highlighted tiles shown in the training sessions do not appear in live authentication sessions on which the users were tested. It is not uncommon for users to either forget or accidently click or type something unexpected into an input feild. For example, in HTML, the text based passwords are entered into `<input type="password">` elements which obscure the password using dots or asterisk. This provides the user some feedback as to how far along they are into the authentication they are and if they have made an error by accidently striking a key, they can see it and immediately erase it and begin again even if they don't know what key was accidently struck. To address this issue our password scheme has incorporated a feedback mechanism which simply reflects the password to the user. We are aware that this poses as significant security vulnerability as shoulder surfers could easy capture passwords. The images can be replaced by agnostic symbols such as an asterisk or dot.

### Screen Images

The home screen showing all the options and the progression of stages participant have to go through in testing the system.

<center><img src="doc/home.png" width="30%" height="30%"></center>
<center>Figure 6: Password home screen and teh to the right of the screen the log data is viewed and populated as participants carry out each task</center>

**Selecting a category**: The categories are presented as a drop down list element to the user. By clicking on the desired category of password the user will be taken to the next page which will generate the password.

<center><img src="doc/category_select.png" width="50%" height="50%"></center>
<center>Figure 7: Password category screen</center>
<center><img src="doc/category_select_list.png" width="50%" height="50%"></center>
<center>Figure 8: Interface showing the drop down list the user has to select a category from.</center>

<center><img src="doc/password_testing.png" width="50%" height="50%"></center>
<center>Figure 9: Interface showing how the user can select a category by clicking on a list item</center>

<center><img src="doc/password_entering.png" width="50%" height="50%"></center>
<center>Figure 10: User entering password. The password is echoed back to the user as the learn the interface</center>

**Password Entry**: The user can begin entering their password as soon as the screen becomes available. When clicking on an image, the output line named "Selected Password" provides the user with feedback regarding their selected pictures. In Figure 10 above the user selected the Statue of Liberty as the first entry into their password which will be four images long. A limitation of the image21 scheme observed and stated in the previous section is not providing the user with information regarding progress when entering a password. This interface shows how we plan to eliminate that limitation by having a reflective-length based feedback interface allowing the user to know and verify what they have entered. The security implications are discussed above.

<center><img src="doc/banking_password_entering.png" width="50%" height="50%"></center>
<center>Figure 11: User entering their Banking password</center>

<center><img src="doc/success_attempt.png" width="50%" height="50%"></center>
<center>Figure 12: Password success screen</center>

<center><img src="doc/failed_attempt.png" width="50%" height="50%"></center>
<center>Figure 13: Alert box telling the user that the login attempt has failed</center>

<center><img src="doc/sample_log.png" width="50%" height="50%"></center>
<center>Figure 14: Sample Log file generated by the testing system</center>

### Comparison with Image21

Testing on 10 users done with the password scheme we developed. Data from the log files generated per user session was anlayzed and processed using the tools as before.

<center>Table 4: The summary statistics of the user studies, showing the standard deviation, mean and median of the successfull logins, unsuccessful logins and login time</center>
<center><img src="doc/user_data_stats.png" width="50%" height="50%"></center>

<center>Table 5: Boxplots showing the median, mean, quartile ranges and outliers of the image21 data and the password scheme developed</center>

|                pec_21                 |               Image 21                |
| :-----------------------------------: | :-----------------------------------: |
|    ![](doc/pec_2019_histogram.png)    |         ![](doc/image21.png)          |
| ![](doc/pec_image21_successlogin.png) | ![](doc/pec_image21_unseccessful.png) |

**Observations**

1. The pec_2019 data consists of 10 participants who form distinctly different population than that of image21: This should be taken into consideration when determining the value of all conclusions made from this study.
2. The pec_2019 has a lower inter-quartile range than image21 as can be seen from the box length of the box-plots in Table 5. This means on average the participants spent less time on the pec_2019 interface authentication with the system than did the image21 participants. In the final steps as pointed out above the user are provided feedback on progress. In addition to this instead of providing the user with 48 tiles of single image, the pec_2019 scheme offers distinct images, which would be easier to distinguish and recall at learning and authentication stages. the pec_2019 scheme also allowed the user to play a role in selecting a password category, allowing the user to be more emotionally involved in the authentication process and promoting greater learning and recollection.
3. The spread of the data as can be seen in the summary table show that, the mean and median for both successful and unsuccessful login attempts, was relativedly close, with values of 18.1 and 13, and 39 and 37 respectively.

**Summary**: The data shows that the pec_2019 passowrd scheme improved usability in terms of improving the authentication times. Overall the color scheme and UI of the entire scheme was mroe appealling than that of image21, which may have been another factor which promoted greater user engagement. In this study user engagement is measured in terms of how quickly the user can make it through the authentication process. The fastest times where from the text21 system, which boasts simplicity and a low learning curve because of its already widespread use. It can also be said that the implemented system did improve the overall usability of image based passwords.
