import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class Database {
  categories: Observable<any[]>;
  cars: Observable<any[]>;
  fruits: Observable<any[]>;
  places: Observable<any[]>;
  social: Observable<any[]>;
  flags: Observable<any[]>;
  constructor(public db: AngularFireDatabase) {
    this.init();
  }

  init() {
    this.getList();
    this.getCars();
    this.getFlags();
    this.getPlaces();
    this.getSocial();
    this.getFruits();
  }

  getList() {
    this.categories = this.db.list('categories').valueChanges();
    return this.categories.pipe(map(res => {
      return res;
    }));

  }

  getCars() {
    this.cars = this.db.list('categories/cars').valueChanges();
    return this.cars.pipe(map(res => {
      return res;
    }));
  }

  getFruits() {
    this.fruits = this.db.list('categories/fruits').valueChanges();
    return this.fruits.pipe(map(res => {
      return res;
    }));
  }

  getPlaces() {
    this.places = this.db.list('categories/places').valueChanges();
    return this.places.pipe(map(res => {
      return res;
    }));
  }

  getSocial() {
    this.social = this.db.list('categories/social').valueChanges();
    return this.social.pipe(map(res => {
      return res;
    }));
  }

  getFlags() {
    this.flags = this.db.list('categories/flags').valueChanges();
    return this.flags.pipe(map(res => {
      return res;
    }));
  }


}