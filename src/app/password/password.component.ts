import { Component, OnInit, Input } from "@angular/core";
import { Database } from "../database";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { catService, catProd } from "../Config";

@Component({
  selector: "app-password",
  templateUrl: "./password.component.html",
  styleUrls: ["./password.component.css"]
})
export class PasswordComponent implements OnInit {
  list: any[];
  @Input() data;
  username: any;
  selected: any[] = new Array();
  category: any;
  password: string;
  testPass: boolean;
  recoverPass: string;
  attempts: number = 0;
  passData: any = {
    type: "",
    startTime: "",
    completionTime: "",
    attempts: 0,
    failedAttempts: 0
  };
  constructor(public db: Database, public activeModal: NgbActiveModal) {}

  ngOnInit() {
    this.db.getList().subscribe(data => {
      var arr = new Array();
      arr = arr.concat(data[0], data[1], data[2], data[3], data[4]);
      this.list = arr;
    });
    var userData = JSON.parse(localStorage.getItem("userData"));
    this.passData.startTime = new Date();
    this.passData.type = this.data.type;
    if (this.data.type == "bank") {
      userData.generatePass.bank = this.passData;
    }
    if (this.data.type == "email") {
      userData.generatePass.email = this.passData;
    }
    if (this.data.type == "instagram") {
      userData.generatePass.instagram = this.passData;
    }
    localStorage.setItem("userData", JSON.stringify(userData));
  }

  recover(recoverPass) {
    var password = new Array();
    this.category.forEach(element => {
      password.push(element.title);
    });
    if (recoverPass == this.password) {
      alert("Password recovered! Your password is " + password);
      localStorage.setItem(this.data.type, JSON.stringify(this.category));

      var userData = JSON.parse(localStorage.getItem("userData"));

      if (this.data.type == "bank") {
        userData.generatePass.bank.completionTime = new Date();
      }
      if (this.data.type == "email") {
        userData.generatePass.email.completionTime = new Date();
      }
      if (this.data.type == "instagram") {
        userData.generatePass.instagram.completionTime = new Date();
      }
      localStorage.setItem("userData", JSON.stringify(userData));

      this.activeModal.close();
    } else {
      alert("Incorrect password, start all over");
      var userData = JSON.parse(localStorage.getItem("userData"));

      if (this.data.type == "bank") {
        userData.generatePass.bank.failedAttempts =
          userData.generatePass.bank.failedAttempts + 1;
      }
      if (this.data.type == "email") {
        userData.generatePass.email.failedAttempts =
          userData.generatePass.email.failedAttempts + 1;
      }
      if (this.data.type == "instagram") {
        userData.generatePass.instagram.failedAttempts =
          userData.generatePass.instagram.failedAttempts + 1;
      }
      localStorage.setItem("userData", JSON.stringify(userData));

      this.activeModal.close();
    }
  }
  tryPass() {
    this.selected = [];
    this.testPass = true;
  }
  retryPass() {
    this.category = [];
    this.password = "";
    this.testPass = false;
    this.attempts = 0;
  }

  getRandomImg(arr) {
    var result = [];
    for (var i = 0; i < 4; i++) {
      var index = Math.floor(Math.random() * arr.length);
      result.push(arr[index]);
      arr.splice(index, 1);
    }
    return result;
  }

  inArray(array, el) {
    for (var i = array.length; i--; ) {
      if (array[i] === el) return true;
    }
    return false;
  }

  isEqArrays(arr1, arr2) {
    var val = JSON.stringify(arr1) === JSON.stringify(arr2);
    return val;
  }
  selectPass(item) {
    if (this.selected.length < 4) {
      this.selected.push(item);
      if (this.selected.length == 4) {
        if (this.isEqArrays(this.selected, this.category)) {
          alert("Password correct! Password Saved");
          catService.info(
            () => `User LOGIN SUCCESS at attempt #${this.attempts}`
          );
          localStorage.setItem(this.data.type, JSON.stringify(this.selected));

          var userData = JSON.parse(localStorage.getItem("userData"));

          if (this.data.type == "bank") {
            userData.generatePass.bank.completionTime = new Date();
          }
          if (this.data.type == "email") {
            userData.generatePass.email.completionTime = new Date();
          }
          if (this.data.type == "instagram") {
            userData.generatePass.instagram.completionTime = new Date();
          }
          localStorage.setItem("userData", JSON.stringify(userData));

          this.activeModal.close();
        } else {
          this.selected = [];

          if (this.attempts < 2) {
            alert("Password incorrect! Retry");
            var userData = JSON.parse(localStorage.getItem("userData"));

            if (this.data.type == "bank") {
              userData.generatePass.bank.failedAttempts =
                userData.generatePass.bank.failedAttempts + 1;
            }
            if (this.data.type == "email") {
              userData.generatePass.email.failedAttempts =
                userData.generatePass.email.failedAttempts + 1;
            }
            if (this.data.type == "instagram") {
              userData.generatePass.instagram.failedAttempts =
                userData.generatePass.instagram.failedAttempts + 1;
            }
            localStorage.setItem("userData", JSON.stringify(userData));
          } else {
          }
          this.attempts++;
          var userData = JSON.parse(localStorage.getItem("userData"));

          if (this.data.type == "bank") {
            userData.generatePass.bank.attempts =
              userData.generatePass.bank.attempts + 1;
          }
          if (this.data.type == "email") {
            userData.generatePass.email.attempts =
              userData.generatePass.email.attempts + 1;
          }
          if (this.data.type == "instagram") {
            userData.generatePass.instagram.attempts =
              userData.generatePass.instagram.attempts + 1;
          }
          localStorage.setItem("userData", JSON.stringify(userData));
        }
      }
    } else {
      this.selected = [];
    }
  }

  randomLetter(str) {
    var text = "";
    for (var i = 0; i < 1; i++)
      text += str.charAt(Math.floor(Math.random() * str.length));

    return text;
  }
  /**
Create the password for the user
 */
  createPass(data) {
    this.category = this.getRandomImg(data);
    var arr: any = [];
    this.category.forEach(element => {
      var title = element.title;
      arr.push({
        name: title,
        letter: this.randomLetter(title)
      });
    });
    var password = (
      arr[0].letter +
      arr[1].letter +
      arr[2].letter +
      arr[3].letter
    )
      .toString()
      .toLowerCase();
    arr.push({ password: password });
    this.password = password;
    catService.info(() => `User CREATING PASSWORD`);
  }

  choose(val) {
    switch (val) {
      case "Cars":
        this.db.getCars().subscribe(data => {
          this.createPass(data);
        });
        break;
      case "Fruits":
        this.db.getFruits().subscribe(data => {
          this.createPass(data);
        });
        break;
      case "Places":
        this.db.getPlaces().subscribe(data => {
          this.createPass(data);
        });
        break;
      case "Social":
        this.db.getSocial().subscribe(data => {
          this.createPass(data);
        });
        break;
      case "Flags":
        this.db.getFlags().subscribe(data => {
          this.createPass(data);
        });
        break;
      default:
        break;
    }
  }
}
