import { Component, OnInit, Input } from "@angular/core";
import { Database } from "../database";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-password",
  templateUrl: "./enterPassword.component.html",
  styleUrls: ["./enterPassword.component.css"]
})
export class EnterPassComponent implements OnInit {
  list: any[];
  @Input() data;
  username: any;
  selected: any[] = new Array();
  category: any;
  password: string;
  testPass: boolean = true;
  recoverPass: string;
  attempts: number = 0;
  passData: any = {
    type: '',
    startTime: '',
    completionTime: '',
    attempts: 0,
    failedAttempts: 0
  }
  constructor(public db: Database, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.db.getList().subscribe(data => {
      var arr = new Array();
      arr = arr.concat(data[0], data[1], data[2], data[3], data[4]);
      this.list = arr;
    });
    this.category = JSON.parse(localStorage.getItem(this.data.type));

    var userData = JSON.parse(localStorage.getItem('userData'));
    this.passData.startTime = new Date();
    this.passData.type = this.data.type;
    if (this.data.type == 'bank') {
      userData.enterPass.bank = this.passData;
    }
    if (this.data.type == 'email') {
      userData.enterPass.email = this.passData;
    }
    if (this.data.type == 'instagram') {
      userData.enterPass.instagram = this.passData;
    }
    localStorage.setItem('userData', JSON.stringify(userData));
  }

  recover(recoverPass) {
    var password = new Array;
    this.category.forEach(element => {
      password.push(element.title)
    });
    if (recoverPass == this.password) {
      alert("Password recovered! Your password is " + password);
      this.activeModal.close();
    }
    else {
      alert("Incorrect password, start all over");
      var userData = JSON.parse(localStorage.getItem('userData'));


      if (this.data.type == 'bank') {
        userData.enterPass.bank.failedAttempts = userData.enterPass.bank.failedAttempts + 1;
      }
      if (this.data.type == 'email') {
        userData.enterPass.email.failedAttempts = userData.enterPass.email.failedAttempts + 1;
      }
      if (this.data.type == 'instagram') {
        userData.enterPass.instagram.failedAttempts = userData.enterPass.instagram.failedAttempts + 1;
      }
      localStorage.setItem('userData', JSON.stringify(userData));
      this.activeModal.close();
    }
  }
  tryPass() {
    this.selected = [];
    this.testPass = true;
  }
  retryPass() {
    this.category = [];
    this.password = "";
    this.testPass = false;
    this.attempts = 0;
  }

  getRandomImg(arr) {
    var result = [];
    for (var i = 0; i < 4; i++) {
      var index = Math.floor(Math.random() * arr.length);
      result.push(arr[index]);
      arr.splice(index, 1);
    }
    return result;
  }

  inArray(array, el) {
    for (var i = array.length; i--;) {
      if (array[i] === el) return true;
    }
    return false;
  }

  isEqArrays(arr1, arr2) {
    var val = JSON.stringify(arr1) === JSON.stringify(arr2);
    return val;
  }
  selectPass(item) {
    if (this.selected.length < 4) {
      this.selected.push(item);
      if (this.selected.length == 4) {
        if (this.isEqArrays(this.selected, this.category)) {
          alert("Password correct!");

          var userData = JSON.parse(localStorage.getItem('userData'));

          if(this.data.type == 'bank'){
            userData.enterPass.bank.completionTime = new Date();
          }
          if(this.data.type == 'email'){
            userData.enterPass.email.completionTime = new Date();
          }
          if(this.data.type == 'instagram'){
            userData.enterPass.instagram.completionTime = new Date();
          }
          localStorage.setItem('userData',JSON.stringify(userData));

          this.activeModal.close();
        } else {
          this.selected = [];

          if (this.attempts < 2) {
            alert("Password incorrect! Retry");
            var userData = JSON.parse(localStorage.getItem('userData'));


            if (this.data.type == 'bank') {
              userData.enterPass.bank.failedAttempts = userData.enterPass.bank.failedAttempts + 1;
            }
            if (this.data.type == 'email') {
              userData.enterPass.email.failedAttempts = userData.enterPass.email.failedAttempts + 1;
            }
            if (this.data.type == 'instagram') {
              userData.enterPass.instagram.failedAttempts = userData.enterPass.instagram.failedAttempts + 1;
            }
            localStorage.setItem('userData', JSON.stringify(userData));

          } else {
            alert('Too many incorrect attempts, recreate password');
            this.activeModal.close();
          }
          this.attempts++;
          var userData = JSON.parse(localStorage.getItem('userData'));
         

          if(this.data.type == 'bank'){
            userData.enterPass.bank.attempts =  userData.enterPass.bank.attempts + 1;
          }
          if(this.data.type == 'email'){
            userData.enterPass.email.attempts = userData.enterPass.email.attempts + 1;
          }
          if(this.data.type == 'instagram'){
            userData.enterPass.instagram.attempts =  userData.enterPass.instagram.attempts +1;
          }
          localStorage.setItem('userData',JSON.stringify(userData));
        }
      }
    } else {
      this.selected = [];
    }
  }



}
