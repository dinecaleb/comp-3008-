import { Component, OnInit } from "@angular/core";
import { Database } from "../database";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PasswordComponent } from "../password/password.component";
import { catService, catProd } from "../Config";
import { EnterPassComponent } from "../enterPassword/enterPassword.component";
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.css"]
})
export class HomepageComponent implements OnInit {
  list: any;
  username: any;
  bankPass:any;
  emailPass:any;
  igPass:any;
  log: any = new Object;
  constructor(public db: Database, private modalService: NgbModal) {
    
  }


  download () {
    this.log = localStorage.getItem('userData');
    let blob = new Blob(['\ufeff' + this.log], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
    dwldLink.setAttribute("target", "_blank");
}
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", "UserLog-Placeholders3008.csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);

    // console.log(this.flattenObject(this.log))
    // new Angular2Csv(this.flattenObject(this.log), 'UserLog-Placeholders3008');
  }

 
  
  ngOnInit() {
    this.username = this.generateUsername();
    this.db.categories.subscribe(data => {
      this.list = data;
    });

    this.bankPass = localStorage.getItem('bank');
    this.emailPass =localStorage.getItem('email');
    this.igPass = localStorage.getItem('instagram');
    this.log = {
      username: this.username,
      enteredSite: new Date(),
      generatePass: {},
      enterPass: {},
    }

    console.log(this.log)
    localStorage.setItem('userData',JSON.stringify(this.log));
  }


  enterPassword(type) {
    const modalRef = this.modalService.open(EnterPassComponent, {
      windowClass: "myCustomModalClass"
    });
    modalRef.componentInstance.data = {
      title: "Enter "+ type  + " password",
      type: type
    };
    modalRef.result.then((data) => {
      this.bankPass = localStorage.getItem('bank');
      this.emailPass =localStorage.getItem('email');
      this.igPass = localStorage.getItem('instagram');
      this.log = JSON.parse(localStorage.getItem('userData'));
      
    }, (reason) => {
      this.bankPass = localStorage.getItem('bank');
      this.emailPass =localStorage.getItem('email');
      this.igPass = localStorage.getItem('instagram');
      this.log = JSON.parse(localStorage.getItem('userData'));

    });
  }

  createPassword(type) {
    const modalRef = this.modalService.open(PasswordComponent, {
      windowClass: "myCustomModalClass"
    });
    modalRef.componentInstance.data = {
      title: "Generate Password",
      type: type, 
    };
    modalRef.result.then((data) => {
      this.bankPass = localStorage.getItem('bank');
      this.emailPass =localStorage.getItem('email');
      this.igPass = localStorage.getItem('instagram');
      this.log = JSON.parse(localStorage.getItem('userData'));
    }, (reason) => {
      this.bankPass = localStorage.getItem('bank');
      this.emailPass =localStorage.getItem('email');
      this.igPass = localStorage.getItem('instagram');
      this.log = JSON.parse(localStorage.getItem('userData'));
    });
  }

  generateUsername() {
    var text = "";

    var charset = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 4; i++)
      text += charset.charAt(Math.floor(Math.random() * charset.length));

    return "user" + text;
  }
}
