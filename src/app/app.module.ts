import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { Route, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { Database } from './database';
import { HomepageComponent } from './homepage/homepage.component';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PasswordComponent } from './password/password.component';
import { EnterPassComponent } from './enterPassword/enterPassword.component';


const ROUTES: any[] = [
  { path: '', component: HomepageComponent},
  { path: 'home', component: HomepageComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PasswordComponent,
    EnterPassComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'my-app'),
    AngularFireDatabaseModule,
    RouterModule.forRoot(ROUTES),
    NgbModule,
  ],
  entryComponents: [PasswordComponent,EnterPassComponent],
  providers: [
    Database
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
